%global goipath github.com/k3s-io/containerd
%global debug_package %{nil}
%global version_suffix k3s1
Version:        1.6.6
Name:           k3s-containerd
Release:        13
Summary:        An industry-standard container runtime
License:        Apache-2.0
URL:            https://github.com/k3s-io/containerd
Source0:        https://github.com/k3s-io/containerd/archive/refs/tags/v%{version}-%{version_suffix}.tar.gz
#source1 from https://github.com/golang/sys version: v0.4.0
Source1:        sys.tar.gz

Patch0:         bbolt-add-loong64-support.patch
Patch0001:      0001-Fix-CVE-2023-25153.patch
Patch0002:      0002-Fix-CVE-2022-23471.patch
Patch0003:      0003-fix-PLATYPUS-attack-of-RAPL-accessible-to-a-containe.patch
Patch0004:      0004-fix-CVE-2023-25173.patch
Patch0005:      0005-fix-CVE-2023-39325.patch
Patch0006:      0006-fix-CVE-2022-41723.patch
Patch0007:      0007-fix-CVE-2024-24786.patch

BuildRequires:  golang glibc-static make btrfs-progs-devel

%description
containerd is an industry-standard container runtime with an emphasis on
simplicity, robustness and portability.  It is available as a daemon for Linux
and Windows, which can manage the complete container lifecycle of its host
system: image transfer and storage, container execution and supervision,
low-level storage and network attachments, etc.

%prep
%autosetup -n containerd-%{version}-%{version_suffix} -p1
%ifarch loongarch64
rm -rf vendor/golang.org/x/sys
tar -xf %{SOURCE1} -C vendor/golang.org/x/
%endif

%build
mkdir -p k3s-containerd/bin

PKG="github.com/k3s-io/k3s"
PKG_CONTAINERD="github.com/containerd/containerd"
PKG_K3S_CONTAINERD="github.com/k3s-io/containerd"
PKG_CRICTL="github.com/kubernetes-sigs/cri-tools/pkg"
PKG_K8S_BASE="k8s.io/component-base"
PKG_K8S_CLIENT="k8s.io/client-go/pkg"
PKG_CNI_PLUGINS="github.com/containernetworking/plugins"

buildDate=$(date -u '+%Y-%m-%dT%H:%M:%SZ')

VERSIONFLAGS="
    -X ${PKG}/pkg/version.Version=v1.24.2+k3s-
    -X ${PKG}/pkg/version.GitCommit=
    -X ${PKG_K8S_CLIENT}/version.gitVersion=v1.24.2+k3s-
    -X ${PKG_K8S_CLIENT}/version.gitCommit=
    -X ${PKG_K8S_CLIENT}/version.gitTreeState=clean
    -X ${PKG_K8S_CLIENT}/version.buildDate=${buildDate}
    -X ${PKG_K8S_BASE}/version.gitVersion=v1.24.2+k3s-
    -X ${PKG_K8S_BASE}/version.gitCommit=
    -X ${PKG_K8S_BASE}/version.gitTreeState=clean
    -X ${PKG_K8S_BASE}/version.buildDate=${buildDate}
    -X ${PKG_CRICTL}/version.Version=v1.24.0-k3s1
    -X ${PKG_CONTAINERD}/version.Version=v1.6.6-k3s1
    -X ${PKG_CONTAINERD}/version.Package=${PKG_K3S_CONTAINERD}
    -X ${PKG_CNI_PLUGINS}/pkg/buildversion.BuildVersion=v1.1.1-k3s1
"
TAGS="apparmor seccomp netgo osusergo providerless urfave_cli_no_docs"
LDFLAGS=" -w -s"
CGO_ENABLED=1 go build -tags "$TAGS" -ldflags "$VERSIONFLAGS $LDFLAGS" -o k3s-containerd/bin ./cmd/containerd
CGO_ENABLED=1 go build -buildmode=pie -tags "$TAGS" -ldflags "$VERSIONFLAGS $LDFLAGS -linkmode=external -extldflags '-Wl,-z,relro -Wl,-z,now'" -o k3s-containerd/bin ./cmd/containerd-shim-runc-v2

%install
install -d -p %{buildroot}%{_libexecdir}/containerd/
cp k3s-containerd/bin/* %{buildroot}%{_libexecdir}/containerd/
mkdir -p %{buildroot}%{_libexecdir}/containerd/containerd-%{version}
cp -rf %{_builddir}/containerd-%{version}-%{version_suffix}/. %{buildroot}%{_libexecdir}/containerd/containerd-%{version} 

%files
%{_libexecdir}/containerd/containerd
%{_libexecdir}/containerd/containerd-shim-runc-v2
%{_libexecdir}/containerd/containerd-%{version}/.


%changelog
* Mon Mar 25 2024 zhangbowei <zhangbowei@kylinos.cn> - 1.6.6-k3s1-13
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC: fix CVE-2024-24786

* Fri Mar 22 2024 zhangbowei <zhangbowei@kylinos.cn> - 1.6.6-k3s1-12
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC: fix CVE-2022-41723

* Thu Mar 21 2024 zhangbowei <zhangbowei@kylinos.cn> - 1.6.6-k3s1-11
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC: fix CVE-2023-39325

* Tue Mar 19 2024 zhangbowei <zhangbowei@kylinos.cn> - 1.6.6-k3s1-10
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC: fix CVE-2023-25173

* Mon Mar 18 2024 zhangbowei <zhangbowei@kylinos.cn> - 1.6.6-k3s1-9
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix PLATYPUS attack of RAPL accessible to a container

* Fri Mar 15 2024 zhangbowei <zhangbowei@kylinos.cn> - 1.6.6-k3s1-8
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC: fix CVE-2022-23471

* Wed Mar 13 2024 zhangbowei <zhangbowei@kylinos.cn> - 1.6.6-k3s1-7
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC: fix CVE-2023-25153

* Wed Jul 12 2023 huajingyun <huajingyun@loongson.cn> - 1.6.6-k3s1-6
- Update sys to v0.4.0

* Tue Jun 06 2023 huajingyun <huajingyun@loongson.cn> - 1.6.6-k3s1-5
- Add loong64 support

* Fri Mar 03 2023 wangkai <wangkai385@h-partners.com> - 1.6.6-k3s1-4
- Add PIE,BIND_NOW,RELRO secure compilation options

* Wed Aug 03 2022 liukuo <liukuo@kylinos.cn> - 1.6.6-k3s1-3
- License compliance rectification

* Tue Aug 02 2022 Ge Wang <wangge20@h-partners.com> - 1.6.6-k3s1-2
- Add src code to package, which is dependended by k3s

* Fri Jul 22 2022 Ge Wang <wangge20@h-partners.com> - 1.6.6-k3s1-1
- Init package
